import express from 'express';

export default class RouterResponse {

    public static success(data: Object, res: express.Response){
        res.json(data);
    }

    public static error(error: string|Object, res: express.Response){
        res.status(400);
        res.json({error: error});
    }

    public static serverError(error: Error, res: express.Response){
        res.status(500);
        res.json({error: error.message});
    }

    public static notFound(res: express.Response){
        res.status(404);
        res.json({status: 'Page NotFound'});
    }
}