import express from 'express';
import RouterResponse from './RouterResponse';
import HangmanController from '../Controllers/HangmanController';

export default class RouterController{

    public routes: express.Router;

    constructor(){
        this.routes = express.Router();
        this.middlewares();
    }

    public middlewares(){
        this.loadRoutes();

        this.routes.use('*', (req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.notFound(res));

        this.routes.use((error: Error, req: express.Request, res: express.Response, next: express.NextFunction) => RouterResponse.serverError(error, res));
    }

    private loadRoutes(){
        this.routes.get('/randomWord', (req, res) => HangmanController.randomWord(req, res));
        this.routes.post('/aLetter', (req, res) => HangmanController.aLetter(req, res));
    }
}