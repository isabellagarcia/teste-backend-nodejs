import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import RouterController from './Router/RouterController';

const app: express.Express = express();
const server: http.Server = http.createServer(app);
const port: number = 4000;
const router: RouterController = new RouterController;

//use cors middleware
app.use(cors());
app.use(bodyParser.json()); // converte body para objeto
app.use(compression()); // compressão de GZip 
app.use(router.routes); // importa rotas do express

server.listen(port, () => {
    console.log('Server start');
})
