import express from 'express';
import RouterResponse from '../Router/RouterResponse';

export default class HangmanController {
    
    private static listWord: Array<any> = [
        {word: 'mosca', tip: 'inseto'},
        {word: 'carinho', tip: 'demonstração de afeto'},
        {word: 'deus', tip: 'sobrenatural'},
        {word: 'janeiro', tip: 'mês'},
        {word: 'brilhante', tip: 'adjetivo'},
        {word: 'universo', tip: 'lugar'},
        {word: 'escola', tip: 'lugar'},
        {word: 'tesoura', tip: 'objeto'},
        {word: 'beijar', tip: 'demonstração de afeto'},
        {word: 'igreja', tip: 'lugar'},
        {word: 'martelo', tip: 'objeto'},
        {word: 'bermuda', tip: 'roupa'},
        {word: 'honesta', tip: 'adjetivo'},
        {word: 'novela', tip: 'programa'},
        {word: 'revista', tip: 'objeto'},
        {word: 'vergonha', tip: 'emoção'},
    ];

    private static wordChosen: any = '';
    private static letterSelected: Array<string> = [];
    private static errorsLength: number = 6;
    private static letters: Array<string> = [];
    private static finish: boolean = false;
    /**
     * randomWord
     *
     * O programa seleciona uma palavra para iniciar o jogo
     *
     * @public
     * @author Isabella Garcia
     * @since  04/2021
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static randomWord(req: express.Request, res: express.Response){
        this.finish = false;
        this.errorsLength = 6;
        this.wordChosen = this.listWord[Math.floor(Math.random() * this.listWord.length)];
        this.letterSelected = [];
        this.letters = [];
        for(let i=0; i < this.wordChosen['word'].length; i++){
            this.letterSelected[i] = '_';
        }


        RouterResponse.success({ 
            success: 'Palavra criada com sucesso!',
            tip: 'A palavra contém ' + this.wordChosen['word'].length + ' letras e a dica é: '
            + this.wordChosen['tip'],
        }, res);
    }

    /**
     * aLetter
     *
     * Insira uma letra e complete a palavra
     *
     * @public
     * @author Isabella Garcia
     * @since  04/2021
     * @param  {express.Request}  req the express request object
     * @param  {express.Response} res the express response object
     * @return {object}
     */
    public static aLetter(req: express.Request, res: express.Response){
        if(this.finish){
            this.restartVariables();
        }

        if(!this.wordChosen){
            RouterResponse.error('Não há uma palavra selecionada.', res);
        }

        let letter = req.body.letter.toLowerCase();
        let indexLetter = this.letters.indexOf(letter);
        if(indexLetter >= 0){
            RouterResponse.success(
                {
                    message: 'Ops! Parece que você já tentou essa letra.',
                    result: 'O jogo está assim: ' + this.letterSelected
                    + '. Tente uma nova letra.'
                }, res);
        }else{
            this.letters.push(letter);
        }        

        let index = this.wordChosen.word.indexOf(letter);

        if(index < 0){
            //consultando se tem mais chances de errar ou não
            if(!this.errorLetter()){
                this.finish = true;
                RouterResponse.success(
                    {
                        message: 'Ops! Parece que você perdeu todas as chances.',
                        result: 'A resposta correta era ' + this.wordChosen['word']
                        + '. Reinicie o jogo.'
                    }, res);
            }else {
                RouterResponse.success(
                    {
                        message: 'Ops! Não há nenhuma letra na palavra. Você agora tem apenas '
                        + this.errorsLength + ' chances de errar.',
                        result: 'O jogo está assim: ' + this.letterSelected + '. Tente uma nova letra.'
                    }, res);
            }
        }else{
            this.letterSelected[index] = letter;
            let result = this.letterSelected.indexOf('_');

            if(result < 0){
                this.finish = true;
                RouterResponse.success(
                    {
                        message: 'Maravilha! Você acertou a palavra!',
                        result: 'A palavra é ' + this.wordChosen.word.toUpperCase()
                        + '. Reinicie o jogo.'
                    }, res);
            }else {
                RouterResponse.success(
                    {
                        message: 'Maravilha! Você encontrou uma letra.',
                        result: 'O jogo está assim: ' + this.letterSelected
                        + '. Tente uma nova letra.'
                    }, res);
            }
           
        }
    }

    /**
     * errorLetter
     *
     * Verifica a quantidade de tentativas frustradas
     *
     * @private
     * @author Isabella Garcia
     * @since  04/2021
     * @return {boolean}
     */
    private static errorLetter(){
        this.errorsLength -= 1;

        if(!this.errorsLength) {
            return false;
        }else{
            return true;
        }
    }

    /**
     * restartVariables
     *
     * Restart de algumas variáveis
     *
     * @private
     * @author Isabella Garcia
     * @since  04/2021
     */
    private static restartVariables(){
        this.letterSelected = [];
        this.wordChosen = '';
        this.letters = [];
    }

}