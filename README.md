# Teste para vaga backend #

Aqui está o passo a passo para rodar a aplicação.

### O que tem? ###

* Uma aplicação backend desenvolvida em NodeJS com typescript.


### Rodando o Back End

```bash
# Clone este repositório
$ git clone <git clone https://isabellagarcia@bitbucket.org/isabellagarcia/teste-backend-nodejs.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd teste-backend-nodejs

# Instale as dependências
$ npm install

# Execute a aplicação
$ npm run build

# O servidor inciará na porta:4000 - acesse <http://localhost:4000>
```

### Como funciona a forca ###

* Depois de executado a aplicação, chame o endpoint <http://localhost:4000/randomWord>
* Este endpoint seleciona uma palavra para começar o jogo.
* Após, chame o enpoint <http://localhost:4000/aLetter> com um objeto {"letter": qualquer letra} no body. Poderá ver melhor no arquivo do Postman disponibilizado nesse repositório.
*Repita o mesmo passo até descobrir a palavra, ou então ter a forca.